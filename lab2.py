import argparse
import tasks.sort as task1
import tasks.generator as task2
import tasks.json as task3
import tasks.nvector as task4
import tasks.logger as task5
import tasks.recursive_dict as task6
import tasks.meta as task7
import tasks.cached as task8
import tasks.myxrange as task9
import tasks.filteredseq as task10


def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('task', type=int, choices=[1, 2, 7, 9], help='task to '
                                                                     'execute')
    args = parser.parse_known_args()
    if args[0].task == 1:
        task1.main(args[1])
    elif args[0].task == 2:
        task2.main(args[1])
    elif args[0].task == 7:
        task7.main(args[1])
    elif args[0].task == 9:
        task9.main(args[1])

if __name__ == '__main__':
    main()