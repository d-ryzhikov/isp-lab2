import math


class NVector(object):
    def __init__(self, *args):
        self.components = []
        for arg in args:
            self.components.append(arg)

    def __add__(self, other):
        if len(self) == len(other):
            return NVector(*[a + b for (a, b)
                             in zip(self.components, other.components)])
        else:
            raise ValueError('vectors must be of equal size')

    def __sub__(self, other):
        if len(self) == len(other):
            return NVector(*[a - b for (a, b)
                             in zip(self.components, other.components)])
        else:
            raise ValueError('vectors must be of equal size')

    def __mul__(self, other):
        if type(self) == type(other):
            return sum([a * b for (a, b)
                        in zip(self.components, other.components)])
        else:
            return NVector(*[x * other for x in self.components])

    def __eq__(self, other):
        return self.components == other

    def length(self):
        return math.sqrt(sum([x ** 2 for x in self.components]))

    def __getitem__(self, item):
        return self.components[item]

    def __len__(self):
        return len(self.components)

    def __str__(self):
        return '{0:d}-vector: {1}'.format(len(self.components),
                                          str(self.components))

    def __repr__(self):
        return '{0:d}-vector: {1}'.format(len(self.components),
                                          str(self.components))
