class MyLogger(object):
    log = ''

    def __getattribute__(self, item):
        obj = object.__getattribute__(self, item)

        def get_log(*args):
            result = obj(*args)
            MyLogger.log += 'Called {0} with args {1}. '\
                            'Result: {2}\n'.format(item, args, result)
            return result

        if callable(obj):
            return get_log

        return object.__getattribute__(self, item)

    def __str__(self):
        return MyLogger.log


class ToLog(MyLogger):
    def __init__(self):
        print 'Hello, World!'

    def foo(self, arg):
        return 'foo(%s)' % str(arg)

    def bar(self, arg):
        return 'bar(%s)' % str(arg)
