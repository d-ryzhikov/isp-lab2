import argparse
import random
import string
import sys


def main(argv):
    args = parse_args(argv)
    file_generator(args.fields_count, args.lines_count, numeric=args.numeric,
                   field_sep=args.field_separator, line_sep=args.line_separator)


def file_generator(
        fields_count, lines_count, numeric=False, field_sep='\t', line_sep='\n'):
    with open('in.txt', 'w') as f:
        lines_ready = 0
        while lines_ready < lines_count:
            fields_ready = 0
            while fields_ready < fields_count:
                if numeric:
                    f.write(''.join(random.choice(string.digits)
                                    for _ in range(6)))
                else:
                    f.write(''.join(random.choice(string.ascii_uppercase +
                                                  string.ascii_lowercase)
                                    for _ in range(6)))
                if fields_ready < fields_count - 1:
                    f.write(field_sep)
                fields_ready += 1
            f.write(line_sep)
            lines_ready += 1


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--lines-count', type=int, default=5,
                        help='Number of lines in the generated file')
    parser.add_argument('-f', '--fields-count', type=int, default=5,
                        help='Number of fields in the generated file')
    parser.add_argument('-ls', '--line-separator', type=str, default='\n',
                        help='Line separator')
    parser.add_argument('-fs', '--field-separator', type=str, default='\t',
                        help='Field separator')
    parser.add_argument('-n', '--numeric', action='store_true', default=False,
                        help='Numeric fields')
    args = parser.parse_args(argv)
    return args

if __name__ == "__main__":
    main(sys.argv[1:])
