class DefaultDict(dict):
    def __getitem__(self, item):
        try:
            return super(DefaultDict, self).__getitem__(item)
        except KeyError:
            self[item] = DefaultDict()
            return self[item]
