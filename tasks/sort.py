import argparse
import os
import sys
import tempfile


def main(argv):
    args = parse_args(argv)
    if args.check:
        print check_big_file(
            input_file=args.input, numeric=args.numeric,
            field_sep=args.field_separator, line_sep=args.line_separator,
            keys=args.keys, reverse=args.reverse, buf_size=args.buffer_size)
    else:
        sort_big_file(
            input_file=args.input, output_file=args.output,
            numeric=args.numeric, field_sep=args.field_separator,
            line_sep=args.line_separator, keys=args.keys, reverse=args.reverse,
            buf_size=args.buffer_size)


def sort_big_file(
        input_file=sys.stdin, output_file=sys.stdout, numeric=False,
        field_sep='\t', line_sep='\n', keys=None, reverse=False,
        buf_size=4096):
    files = split_file(input_file, line_sep=line_sep, buf_size=buf_size)
    for f in files:
        sort_small_file(f, field_sep=field_sep, line_sep=line_sep,
                        buf_size=buf_size, keys=keys, numeric=numeric,
                        reverse=reverse)
    file_pairs = list_to_pairs(files)
    while len(file_pairs) > 1:
        file_pairs = list_to_pairs(files)
        merged_files = []
        for pair in file_pairs:
            merged_files.append(merge_files(
                pair[0], pair[1], line_sep=line_sep, keys=keys,
                buf_size=buf_size, reverse=reverse, numeric=numeric))
        file_pairs = list_to_pairs(merged_files)
    merge_files(
        file_pairs[0][0], file_pairs[0][1], output_file=output_file,
        buf_size=buf_size, line_sep=line_sep, keys=keys,
        reverse=reverse)


def check_big_file(
        input_file=sys.stdin, numeric=False, field_sep='\t', line_sep='\n',
        keys=None, reverse=False, buf_size=4096):
    lines = []
    curr_size = 0
    iterator = delimit(input_file, line_sep=line_sep, buf_size=buf_size)
    line = next(iterator, None)
    while line:
        while (curr_size + len(line) < buf_size) and line:
            lines.append(line)
            curr_size += len(line)
            line = next(iterator, None)
        if check_lines(lines, field_sep=field_sep, line_sep=line_sep,
                       keys=keys, reverse=reverse, numeric=numeric):
            lines[:-1] = lines[-1]
            curr_size = len(lines[0])
        else:
            return False
    else:
        return True


def check_lines(
        lines, field_sep='\t', line_sep='\n', keys=None, numeric=False,
        reverse=False):

    def compare(i, j):
        if reverse:
            if keys:
                if numeric:
                    return (
                        [int(lines[i].rstrip(line_sep).split(field_sep)[k])
                         for k in keys] >=
                        [int(lines[j].rstrip(line_sep).split(field_sep)[k])
                         for k in keys])
                else:
                    return (
                        [lines[i].rstrip(line_sep).split(field_sep)[k]
                         for k in keys] >=
                        [lines[j].rstrip(line_sep).split(field_sep)[k]
                         for k in keys])
            else:
                if numeric:
                    return (
                        [int(item) for item
                         in lines[i].rstrip(line_sep).split(field_sep)] >=
                        [int(item) for item
                         in lines[j].rstrip(line_sep).split(field_sep)])
                else:
                    return (
                        [item for item
                         in lines[i].rstrip(line_sep).split(field_sep)] >=
                        [item for item
                         in lines[j].rstrip(line_sep).split(field_sep)])
        else:
            if keys:
                if numeric:
                    return (
                        [int(lines[i].rstrip(line_sep).split(field_sep)[k])
                         for k in keys] <=
                        [int(lines[j].rstrip(line_sep).split(field_sep)[k])
                         for k in keys])
                else:
                    return (
                        [lines[i].rstrip(line_sep).split(field_sep)[k]
                         for k in keys] <=
                        [lines[j].rstrip(line_sep).split(field_sep)[k]
                         for k in keys])
            else:
                if numeric:
                    return (
                        [int(item) for item
                         in lines[i].rstrip(line_sep).split(field_sep)] <=
                        [int(item) for item
                         in lines[j].rstrip(line_sep).split(field_sep)])
                else:
                    return (
                        [item for item
                         in lines[i].rstrip(line_sep).split(field_sep)] <=
                        [item for item
                         in lines[j].rstrip(line_sep).split(field_sep)])

    for x in xrange(len(lines) - 1):
        if not compare(x, x+1):
            return False
    else:
        return True


def split_file(f, line_sep='\n', buf_size=4096):
    files = []
    buf = ''
    for line in delimit(f, line_sep=line_sep, buf_size=buf_size):
        if (len(buf)+len(line)) <= buf_size:
            buf += line
        else:
            files.append(tempfile.mkstemp()[1])
            with open(files[-1], 'w') as f:
                f.write(buf)
            buf = ''
    if buf:
        files.append(tempfile.mkstemp()[1])
        with open(files[-1], 'w') as f:
            f.write(buf)
    return files


def delimit(f, line_sep='\n', buf_size=4096):
    buf = ''
    while True:
        new_buf = f.read(buf_size)
        if not new_buf:
            if buf.rstrip(line_sep):
                yield buf
            return
        buf += new_buf
        lines = buf.split(line_sep)
        for line in lines[:-1]:
            yield line + line_sep
        buf = lines[-1]


def sort_small_file(
        f, field_sep='\t', line_sep='\n', buf_size=4096, keys=None,
        numeric=False, reverse=False):
    with open(f, 'r+') as f:
        lines = list(delimit(f, line_sep=line_sep, buf_size=buf_size))
        lines = sort_lines(lines, field_sep=field_sep, line_sep=line_sep,
                           keys=keys, numeric=numeric, reverse=reverse)
        f.seek(0)
        for line in lines:
            f.write(line)


def merge_files(
        file_1, file_2, output_file=None, buf_size=4096, line_sep='\n',
        keys=None, numeric=False, reverse=False):
    file_1 = open(file_1, 'r')

    if not file_2:
        if output_file:
            for lines in delimit(file_1, line_sep=line_sep, buf_size=buf_size):
                output_file.write(lines)
            return
        else:
            return file_1.name
    file_2 = open(file_2, 'r')
    iter_1 = delimit(file_1, line_sep=line_sep, buf_size=buf_size)
    iter_2 = delimit(file_2, line_sep=line_sep, buf_size=buf_size)
    line_1 = next(iter_1, None)
    line_2 = next(iter_2, None)
    if output_file:
        result = output_file
    else:
        result = open(tempfile.mkstemp()[1], 'w')
    while line_1 and line_2:
        lines = sort_lines([line_1, line_2], line_sep=line_sep, keys=keys,
                           numeric=numeric, reverse=reverse)
        result.write(lines[0])
        if lines[0] == line_1:
            line_1 = next(iter_1, None)
        else:
            line_2 = next(iter_2, None)
    while line_1:
        result.write(line_1)
        line_1 = next(iter_1, None)
    while line_2:
        result.write(line_2)
        line_2 = next(iter_2, None)
    os.remove(file_1.name)
    os.remove(file_2.name)
    if not output_file:
        return result.name


def sort_lines(
        lines, field_sep='\t', line_sep='\n', keys=None, numeric=False,
        reverse=False):

    def get_key(line):
        items = line.rstrip(line_sep).rstrip(field_sep).split(field_sep)
        if numeric:
            if keys:
                return [int(items[key]) for key in keys]
            else:
                return [int(item) for item in items]
        else:
            if keys:
                return [items[key] for key in keys]
            else:
                return items

    return sorted(lines, key=get_key, reverse=reverse)


def list_to_pairs(lst):
    result = zip(lst[::2], lst[1::2])
    if len(lst) % 2 != 0:
        result.append((lst[-1], None))
    return result


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        default=sys.stdout)
    parser.add_argument('-c', '--check', action='store_true')
    parser.add_argument('-n', '--numeric', action='store_true')
    parser.add_argument('-r', '--reverse', action='store_true')
    parser.add_argument('-l', '--line-separator', type=str, default='\n')
    parser.add_argument('-f', '--field-separator', type=str, default='\t')
    parser.add_argument('-k', '--keys', type=int, nargs='+')
    parser.add_argument('--buffer-size', type=int, default=4096)
    args = parser.parse_args(argv)
    return args

if __name__ == "__main__":
    main(sys.argv[1:])
