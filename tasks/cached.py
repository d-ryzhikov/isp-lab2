def cached(foo):
    def check_args(*args):
        try:
            if str(args) in check_args.previous_args:
                return check_args.previous_args[str(args)]
            else:
                check_args.previous_args[str(args)] = foo(*args)
                return check_args.previous_args[str(args)]
        except AttributeError:
            check_args.previous_args = {str(args): foo(*args)}

    return check_args
