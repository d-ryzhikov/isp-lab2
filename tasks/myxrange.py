import argparse
import sys


class MyXrange(object):
    def __init__(self, *args):
        if len(args) == 1:
            start = 0
            end = args[0]
            step = 1
        elif len(args) == 2:
            start = args[0]
            end = args[1]
            step = 1
        elif len(args) == 3:
            start = args[0]
            end = args[1]
            step = args[2]
        else:
            raise TypeError('xrange() requires 1-3 int arguments')

        try:
            self._start, self._end, self._step = (int(start), int(end),
                                                  int(step))
        except ValueError:
            raise TypeError('an integer is required')

        if self._step == 0:
            raise ValueError('MyXrange() arg 3 must not be zero')

    def __iter__(self): return self

    def next(self):
        if self._step > 0:
            if self._start >= self._end:
                raise StopIteration
        else:
            if self._start <= self._end:
                raise StopIteration
        current = self._start
        self._start += self._step
        return current


def main(argv):
    args = parse_args(argv)
    if args.start is None:
        print list(MyXrange(args.end))
    elif args.step is None:
        print list(MyXrange(args.start, args.end))
    else:
        print list(MyXrange(args.start, args.end, args.step))


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', type=int)
    parser.add_argument('--end', type=int, required=True)
    parser.add_argument('--step', type=int)
    return parser.parse_args(argv)

if __name__ == "__main__":
    main(sys.argv[1:])
