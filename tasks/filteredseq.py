class FilteredSequence(object):
    def __init__(self, seq):
        self._object = [item for item in seq]

    def __iter__(self):
        return iter(self._object)

    def filter(self, foo):
        for i in xrange(len(self._object)):
            if foo(self._object[i]):
                yield self._object[i]
