import argparse
import sys


def create_meta(f):
    class MyMeta(type):
        def __new__(mcs, name, bases, dct):
            for line in f:
                values = line.split('=')
                dct[values[0]] = eval(values[1])
            return super(MyMeta, mcs).__new__(mcs, name, bases, dct)
    return MyMeta


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=argparse.FileType('r'),
                        default=open('file2class.txt', 'r'))
    return parser.parse_args(argv)


def main(argv):
    class TestMyMeta(object):
        __metaclass__ = create_meta(parse_args(argv).file)
    print TestMyMeta.__dict__

if __name__ == "__main__":
    main(sys.argv[1:])
